const char help[] = "Test driver for the correctness of Gauss-Seidel iteration implementation";

#include <petscmat.h>
#include <curand.h>
#include <cublas_v2.h>
#include "gs.h"

#define CUDA_CHK(cerr) do {cudaError_t _cerr = (cerr); if ((_cerr) != cudaSuccess) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_LIB,"Cuda error %s", cudaGetErrorString(_cerr));} while(0)
#define CURAND_CHK(crs) do {curandStatus_t _crs = (crs); if ((_crs) != CURAND_STATUS_SUCCESS) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_LIB,"Curand error");} while(0)
#define CUBLAS_CHK(cbs) do {cublasStatus_t _cbs = (cbs); if ((_cbs) != CUBLAS_STATUS_SUCCESS) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_LIB,"Cublas error");} while(0)

const __constant__ double v[3][3][3] = {{{-1./12., -1./6. , -1./12.},
                                         {-1./6. ,  0.    , -1./6. },
                                         {-1./12., -1./6. , -1./12.}},
                                        {{-1./6. ,  0.    , -1./6. },
                                         { 0.    ,  8./3. ,  0.    },
                                         {-1./6. ,  0.    , -1./6. }},
                                        {{-1./12., -1./6. , -1./12.},
                                         {-1./6. ,  0.    , -1./6. },
                                         {-1./12., -1./6. , -1./12.}}};

__global__ void packBuffer(size_t uXS, size_t uXE, size_t uYS, size_t uYE, size_t uZS, size_t uZE, size_t pXS, size_t pXE, size_t pYS, size_t pYE, size_t pZS, size_t pZE, double *u, double *buffer)
{
  int myI = threadIdx.x + blockDim.x * blockIdx.x;
  int myJ = threadIdx.y + blockDim.y * blockIdx.y;
  int myK = threadIdx.z + blockDim.z * blockIdx.z;

  if (myI < (pXE - pXS) && myJ < (pYE - pYS) && myK < (pZE - pZS)) {
    int uI = myI + pXS - uXS;
    int uJ = myJ + pYS - uYS;
    int uK = myK + pZS - uZS;

    size_t bIndex = myI + (pXE - pXS) * (myJ + (pYE - pYS) * myK);
    size_t uIndex = uI + (uXE - uXS) * (uJ + (uYE - uYS) * uK);

    buffer[bIndex] = u[uIndex];
  }
}

__global__ void unpackBuffer(size_t uXS, size_t uXE, size_t uYS, size_t uYE, size_t uZS, size_t uZE, size_t pXS, size_t pXE, size_t pYS, size_t pYE, size_t pZS, size_t pZE, double *u, double *buffer)
{
  int myI = threadIdx.x + blockDim.x * blockIdx.x;
  int myJ = threadIdx.y + blockDim.y * blockIdx.y;
  int myK = threadIdx.z + blockDim.z * blockIdx.z;

  if (myI < (pXE - pXS) && myJ < (pYE - pYS) && myK < (pZE - pZS)) {
    int uI = myI + pXS - uXS;
    int uJ = myJ + pYS - uYS;
    int uK = myK + pZS - uZS;

    size_t bIndex = myI + (pXE - pXS) * (myJ + (pYE - pYS) * myK);
    size_t uIndex = uI + (uXE - uXS) * (uJ + (uYE - uYS) * uK);

    u[uIndex] = buffer[bIndex];
  }
}

__global__ void stencilApply(size_t n, double h, size_t uXS, size_t uXE, size_t uYS, size_t uYE, size_t uZS, size_t uZE, double *u, double **halo, double *f)
{
  int myI = threadIdx.x + blockDim.x * blockIdx.x;
  int myJ = threadIdx.y + blockDim.y * blockIdx.y;
  int myK = threadIdx.z + blockDim.z * blockIdx.z;

  if (myI < (uXE - uXS) && myJ < (uYE - uYS) && myK < (uZE - uZS)) {
    int gi = myI + uXS;
    int gj = myJ + uYS;
    int gk = myK + uZS;
    int i, j, k;

    double x[3][3][3];
    for (k = gk-1; k <= gk+1; k++) {
      int hk;

      if (k < 0 || k >= n) continue;
      hk = k < uZS ? 0 : k < uZE ? 1 : 2;
      for (j = gj-1; j <= gj+1; j++) {
        int hj;

        if (j < 0 || j >= n) continue;
        hj = j < uYS ? 0 : j < uYE ? 1 : 2;

        for (i = gi-1; i <= gi+1; i++) {
          int hi;

          if (i < 0 || i >= n) continue;
          hi = i < uXS ? 0 : i < uXE ? 1 : 2;

          if (hi == 1 && hj == 1 && hk == 1) {
            x[k-(gk-1)][j-(gj-1)][i-(gi-1)] = u[(i-uXS) + (uXE-uXS)*((j-uYS) + (uYE-uYS)*(k-uZS))];
          } else {
            int kidx = (hk == 1) ? k-uZS : 0;
            int jidx = (hj == 1) ? j-uYS : 0;
            int jdim = (hj == 1) ? (uYE - uYS) : 1;
            int iidx = (hi == 1) ? i-uXS : 0;
            int idim = (hi == 1) ? (uXE - uXS) : 1;

            if (hk == 1 && hj == 2 && hi == 1) {
              x[k-(gk-1)][j-(gj-1)][i-(gi-1)] = halo[9 * hk + 3 * hj + hi][iidx + idim * (jidx + jdim * kidx)];
            }
          }
        }
      }
    }
    f[myI + (uXE-uXS)*(myJ + (uYE-uYS)*(myK))] = 0.;
    for (k = 0; k < 3; k++) {
      for (j = 0; j < 3; j++) {
        for (i = 0; i < 3; i++) {
          f[myI + (uXE-uXS)*(myJ + (uYE-uYS)*(myK))] += x[k][j][i] * v[k][j][i] * h;
        }
      }
    }
  }
}

static PetscErrorCode StencilMatApply(PetscInt n, PetscInt numDevices, struct IndexBlock *ibAll, double **u, double**f, PetscBool debug)
{
  int d;
  cudaError_t cerr;
  PetscErrorCode ierr;

  PetscFunctionBegin;

  for (d = 0; d < numDevices; d++) {
    struct IndexBlock *ib = &ibAll[d];
    PetscInt       nm1 = n - 1;

    PetscReal      h = 1. / n;
    PetscInt       i, j, k;
    size_t         xS = ib->xStart;
    size_t         xE = ib->xEnd;
    size_t         yS = ib->yStart;
    size_t         yE = ib->yEnd;
    size_t         zS = ib->zStart;
    size_t         zE = ib->zEnd;
    PetscInt       nx = PetscMax((PetscInt) xE - (PetscInt) xS,0);
    PetscInt       ny = PetscMax((PetscInt) yE - (PetscInt) yS,0);
    PetscInt       nz = PetscMax((PetscInt) zE - (PetscInt) zS,0);
    PetscMPIInt    rank;
    int            p;
    double         *haloValues[3][3][3];

    cerr = cudaSetDevice(d); CUDA_CHK(cerr);
    rank = d;

    if (!(nx * ny * nz)) {
      continue;
    }
    
    /* Create arrays of the halos */
    for (k = 0; k < 3; k++) {
      PetscInt haloZS = -1, haloZE = -1;

      if (k == 0 && zS > 0) {
        haloZS = zS - 1;
        haloZE = zS;
      } else if (k == 2 && zE < (size_t) n - 1) {
        haloZS = zE;
        haloZE = zE + 1;
      } else if (k == 1 && zE > zS) {
        haloZS = zS;
        haloZE = zE;
      }
      for (j = 0; j < 3; j++) {
        PetscInt haloYS = -1, haloYE = -1;

        if (j == 0 && yS > 0) {
          haloYS = yS - 1;
          haloYE = yS;
        } else if (j == 2 && yE < (size_t) n - 1) {
          haloYS = yE;
          haloYE = yE + 1;
        } else if (j == 1 && yE > yS) {
          haloYS = yS;
          haloYE = yE;
        }

        for (i = 0; i < 3; i++) {
          PetscInt haloXS = -1, haloXE = -1;
          PetscInt haloSize;

          haloValues[k][j][i] = NULL;
          if (k == 1 && j == 1 && i == 1) {
            continue;
          }

          if (i == 0 && xS > 0) {
            haloXS = xS - 1;
            haloXE = xS;
          } else if (i == 2 && xE < (size_t) n - 1) {
            haloXS = xE;
            haloXE = xE + 1;
          } else if (i == 1 && xE > xS) {
            haloXS = xS;
            haloXE = xE;
          }

          haloSize = (haloXE - haloXS) * (haloYE - haloYS) * (haloZE - haloZS);
          if (!haloSize) continue;
          if (debug) {
            ierr = PetscPrintf(PETSC_COMM_SELF, "[%D] halo (%D,%D,%D) is [%D,%D)x[%D,%D)x[%D,%D)\n", rank, i, j, k, haloXS, haloXE, haloYS, haloYE, haloZS, haloZE); CHKERRQ(ierr);
          }
          cerr = cudaMalloc(&haloValues[k][j][i],haloSize * sizeof(double)); CUDA_CHK(cerr);

          for (p = 0; p < numDevices; p++) {
            struct IndexBlock *pib = &ibAll[p];
            if ((PetscInt) pib->xStart < haloXE && haloXS < (PetscInt) pib->xEnd &&
                (PetscInt) pib->yStart < haloYE && haloYS < (PetscInt) pib->yEnd &&
                (PetscInt) pib->zStart < haloZE && haloZS < (PetscInt) pib->zEnd) {
              double *haloBuffer, *haloBufferRecv;
              PetscInt pXS, pXE, pYS, pYE, pZS, pZE;
              dim3 block, grid;
              PetscInt bufSize;

              pXS = PetscMax((PetscInt) pib->xStart, haloXS);
              pYS = PetscMax((PetscInt) pib->yStart, haloYS);
              pZS = PetscMax((PetscInt) pib->zStart, haloZS);
              pXE = PetscMin((PetscInt) pib->xEnd, haloXE);
              pYE = PetscMin((PetscInt) pib->yEnd, haloYE);
              pZE = PetscMin((PetscInt) pib->zEnd, haloZE);

              bufSize = (pXE - pXS) * (pYE - pYS) * (pZE - pZS);

              cerr = cudaMalloc(&haloBufferRecv, bufSize * sizeof(double)); CUDA_CHK(cerr);
              cerr = cudaSetDevice(p); CUDA_CHK(cerr);
              cerr = cudaMalloc(&haloBuffer, bufSize * sizeof(double)); CUDA_CHK(cerr);
              block = dim3(PetscMin(32,pXE-pXS),PetscMin(32,pYE-pYS),PetscMin(32,pZE-pZS));
              grid = dim3((pXE-pXS + block.x - 1) / block.x,(pYE-pYS + block.y - 1) / block.y,(pZE-pZS + block.z - 1) / block.z);
              packBuffer<<<grid, block>>>(pib->xStart,pib->xEnd,pib->yStart,pib->yEnd,pib->zStart,pib->zEnd,pXS,pXE,pYS,pYE,pZS,pZE,u[p],haloBuffer);
              cerr = cudaDeviceSynchronize(); CUDA_CHK(cerr);
              cudaMemcpyPeer(haloBufferRecv,d,haloBuffer,p,bufSize * sizeof(double)); CUDA_CHK(cerr);
              cerr = cudaFree(haloBuffer); CUDA_CHK(cerr);
              cerr = cudaSetDevice(d); CUDA_CHK(cerr);
              unpackBuffer<<<grid, block>>>(haloXS,haloXE,haloYS,haloYE,haloZS,haloZE,pXS,pXE,pYS,pYE,pZS,pZE,haloValues[k][j][i],haloBufferRecv);
              cerr = cudaDeviceSynchronize(); CUDA_CHK(cerr);
              cerr = cudaFree(haloBufferRecv); CUDA_CHK(cerr);
            }
          }
        }
      }
    }

    { /* apply the stencil */
      dim3 block, grid;

      double **halo_d;

      cerr = cudaMalloc(&halo_d,27 * sizeof(double *)); CUDA_CHK(cerr);
      cerr = cudaMemcpy(halo_d, &haloValues[0][0][0], 27 * sizeof(double *), cudaMemcpyHostToDevice); CUDA_CHK(cerr);
      block = dim3(PetscMin(32,nx),PetscMin(32,ny),PetscMin(32,nz));
      grid = dim3((nx + block.x - 1) / block.x,(ny + block.y - 1) / block.y,(nz + block.z - 1) / block.z);
      stencilApply<<<grid, block>>>(nm1,h,ib->xStart,ib->xEnd,ib->yStart,ib->yEnd,ib->zStart,ib->zEnd,u[d],halo_d,f[d]);
      cerr = cudaDeviceSynchronize(); CUDA_CHK(cerr);
      cerr = cudaFree(halo_d); CUDA_CHK(cerr);
    }

    /* Clean up */
    for (k = 0; k < 3; k++) {
      for (j = 0; j < 3; j++) {
        for (i = 0; i < 3; i++) {
          cerr = cudaFree(haloValues[k][j][i]); CUDA_CHK(cerr);
        }
      }
    }
  }
  PetscFunctionReturn(0);
}

static PetscErrorCode GSTest(MPI_Comm comm, GS gs, PetscInt n, PetscInt nits, int numDevices, struct IndexBlock *ib, double **f, double **u, double **uCheck, PetscInt nCheck, double *avgTime, double *relErr, PetscBool debug)
{
  cudaError_t cerr;
  double totalTime = 0.;
  PetscInt     i, d;
  PetscScalar  errorNorm = 0., uNorm = 0.;
  PetscErrorCode ierr;
  cublasHandle_t *handle;
  PetscFunctionBegin;

  ierr = PetscMalloc1(numDevices, &handle); CHKERRQ(ierr);
  for (d = 0; d < numDevices; d++) {
    cublasStatus_t cbs;
    int localSize = (ib[d].xEnd - ib[d].xStart) * (ib[d].yEnd - ib[d].yStart) * (ib[d].zEnd - ib[d].zStart);
    double uNormD;

    cerr = cudaSetDevice(d); CUDA_CHK(cerr);
    cbs = cublasCreate(&handle[d]); CUBLAS_CHK(cbs);
    cbs = cublasDnrm2(handle[d],localSize,uCheck[d],1,&uNormD); CUBLAS_CHK(cbs);
    uNorm += uNormD * uNormD;
  }
  cerr = cudaSetDevice(0); CUDA_CHK(cerr);
  uNorm = PetscSqrtScalar(uNorm);
  ierr = StencilMatApply(n, numDevices, ib, uCheck, f, debug); CHKERRQ(ierr);
  for (i = 0; i < nCheck + 1; i++) {
    double tic, toc, total, maxTotal = 0.;

    for (d = 0; d < numDevices; d++) {
      size_t localSize = (ib[d].xEnd - ib[d].xStart) * (ib[d].yEnd - ib[d].yStart) * (ib[d].zEnd - ib[d].zStart);

      cerr = cudaSetDevice(d); CUDA_CHK(cerr);
      cudaMemset(u[d], 0, localSize * sizeof(double)); CHKERRQ(ierr);
    }
    cerr = cudaSetDevice(0); CUDA_CHK(cerr);
    ierr = MPI_Barrier(comm); CHKERRQ(ierr);
    tic = MPI_Wtime();
    ierr = GSIterateDevice(gs, n, nits, numDevices, ib, (const double **) f, u); CHKERRQ(ierr);
    cerr = cudaDeviceSynchronize(); CHKERRQ(ierr);
    toc = MPI_Wtime();
    total = toc - tic;
    ierr = MPI_Allreduce(&total, &maxTotal, 1, MPI_DOUBLE, MPI_MAX, comm); CHKERRQ(ierr);
    if (i) {totalTime += maxTotal;}
  }
  for (d = 0; d < numDevices; d++) {
    cublasStatus_t cbs;
    int localSize = (ib[d].xEnd - ib[d].xStart) * (ib[d].yEnd - ib[d].yStart) * (ib[d].zEnd - ib[d].zStart);
    double eNormD;
    double minusOne = -1.;

    cerr = cudaSetDevice(d); CUDA_CHK(cerr);
    cerr = cudaMemcpy(f[d],u[d],localSize * sizeof(double), cudaMemcpyDeviceToDevice); CUDA_CHK(cerr);
    cbs = cublasDaxpy(handle[d], localSize, &minusOne, uCheck[d], 1, f[d], 1); CUBLAS_CHK(cbs);
    cbs = cublasDnrm2(handle[d], localSize, f[d],1,&eNormD); CUBLAS_CHK(cbs);
    errorNorm += eNormD * eNormD;
  }
  cerr = cudaSetDevice(0); CUDA_CHK(cerr);
  errorNorm = PetscSqrtScalar(errorNorm);
  *relErr = errorNorm / uNorm;
  *avgTime = totalTime / nCheck;
  for (d = 0; d < numDevices; d++) {
    cublasStatus_t cbs;

    cerr = cudaSetDevice(d); CUDA_CHK(cerr);
    cbs = cublasDestroy(handle[d]); CUBLAS_CHK(cbs);
  }
  cerr = cudaSetDevice(0); CUDA_CHK(cerr);
  ierr = PetscFree(handle);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

int main(int argc, char **argv)
{
  PetscInt       tests[4] = {17, 65, 257, 1025}, test;
  PetscScalar    edps[4];
  PetscInt       numTests = 4;
  PetscInt       nits = 3, nCheck = 10;
  PetscBool      debug;
  PetscRandom    rand;
  MPI_Comm       comm;
  PetscViewer    viewer;
  cudaError_t    cerr;
  int            numDevices;
  double         avgTime, relErr;
  PetscErrorCode ierr;

  ierr = PetscInitialize(&argc, &argv, NULL, help); if (ierr) return ierr;
  comm = PETSC_COMM_WORLD;
  viewer = PETSC_VIEWER_STDOUT_WORLD;
  ierr = PetscOptionsBegin(comm, NULL, "Gauss Seidel Iteration Test Options", "test_gs.c");CHKERRQ(ierr);
  ierr = PetscOptionsIntArray("-tests", "Test sizes to run", "test_gs.c", tests, &numTests, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-num_its", "The number of iterations per test", "test_gs.c", nits, &nits, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-num_time", "The number of times a test is timed", "test_gs.c", nCheck, &nCheck, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-debug", "debug statements for this rank", "test_gs.c", debug, &debug, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);

  if (!numTests) {numTests = 4;}

  ierr = PetscRandomCreate(comm, &rand);CHKERRQ(ierr);
  ierr = PetscRandomSetFromOptions(rand);CHKERRQ(ierr);
  ierr = PetscRandomSeed(rand);CHKERRQ(ierr);

  cerr = cudaGetDeviceCount(&numDevices); CUDA_CHK(cerr);

  ierr = PetscViewerASCIIPrintf(viewer, "Running %D tests of Gauss Seidel iteration with %d devices\n", numTests, numDevices);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);
  for (test = 0; test < numTests; test++) {
    PetscScalar **f, **u, **ucheck;
    PetscInt    n, nits = 1;
    GS          gs = NULL;
    int         d;
    struct IndexBlock *ib;
    size_t      localSize;

    ierr = PetscViewerASCIIPrintf(viewer, "Test %D:\n", test);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);


    ierr = GSCreate(comm, &gs); CHKERRQ(ierr);

    n = tests[test];

    ierr = PetscViewerASCIIPrintf(viewer, "Test dimensions: %D cells (%D dofs) per dimension, %D iterations\n", n, n-1, nits);CHKERRQ(ierr);

    ierr = PetscMalloc4(numDevices,&f,numDevices,&u,numDevices,&ucheck,numDevices,&ib);CHKERRQ(ierr);
    ierr = GSGetFieldArraysDevice(gs, n, numDevices, ib, f, u); CHKERRQ(ierr);

    for (d = 0; d < numDevices; d++) {
      curandStatus_t    crs;
      curandGenerator_t gen;

      cerr = cudaSetDevice(d); CUDA_CHK(cerr);
      crs = curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_DEFAULT); CURAND_CHK(crs);
      crs = curandSetPseudoRandomGeneratorSeed(gen,d); CURAND_CHK(crs);
      localSize = (ib[d].xEnd - ib[d].xStart) * (ib[d].yEnd - ib[d].yStart) * (ib[d].zEnd - ib[d].zStart);
      cerr = cudaMalloc(&ucheck[d], localSize * sizeof(double)); CUDA_CHK(cerr);
      crs = curandGenerateUniformDouble(gen, ucheck[d], localSize); CURAND_CHK(crs);
      crs = curandDestroyGenerator(gen); CURAND_CHK(crs);
    }
    cerr = cudaSetDevice(0); CUDA_CHK(cerr);

    ierr = PetscViewerASCIIPrintf(viewer, "Test random solution, %D instances of %D iterations\n", nCheck, nits);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);
    ierr = GSTest(comm, gs, n, nits, numDevices, ib, f, u, ucheck, nCheck, &avgTime, &relErr, debug); CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer, "Test %D, random solution, %D dofs per dimension: avg %g seconds for %D iterations, relative error %g, ***%g equation digits solved per second***\n", test, n - 1, (double) avgTime, nits, (double) relErr, (double) -log10(relErr) * (n-1)*(n-1)*(n-1) / avgTime);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);

    edps[test] = -log10(relErr) * (n-1)*(n-1)*(n-1) / avgTime;

    for (d = 0; d < numDevices; d++) {
      cerr = cudaSetDevice(d); CUDA_CHK(cerr);
      cerr = cudaFree(ucheck[d]); CUDA_CHK(cerr);
    }
    cerr = cudaSetDevice(0); CUDA_CHK(cerr);

    ierr = GSRestoreFieldArraysDevice(gs, n, numDevices, ib, f, u); CHKERRQ(ierr);

    ierr = GSDestroy(&gs);CHKERRQ(ierr);

    ierr = PetscFree4(f,u,ucheck,ib);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);
  }
  ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);
  ierr = PetscRandomDestroy(&rand);CHKERRQ(ierr);

  {
    double harmonicMean = 0.;

    for (test = 0; test < numTests; test++) {
      harmonicMean += 1./(edps[test] + PETSC_SMALL);
    }
    harmonicMean = numTests / harmonicMean;

    ierr = PetscViewerASCIIPrintf(viewer, "===Harmonic mean: %g equation digits solved per second===\n", harmonicMean); CHKERRQ(ierr);
  }

  ierr = PetscFinalize();
  return ierr;
}
