
#include <petscsys.h>
#include "gs.h"

#define CUDA_CHK(cerr) do {cudaError_t _cerr = (cerr); if ((_cerr) != cudaSuccess) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_LIB,"Cuda error");} while(0)

int GSGetFieldArraysDevice(GS gs, size_t N, int numDevices,
                           struct IndexBlock iBlock[],
                           double *fLocal_p[], double *uLocal_p[])
{
  cudaError_t       cerr;

  PetscFunctionBeginUser;

  iBlock[0].xStart = 0;
  iBlock[0].xEnd   = N - 1;
  iBlock[0].yStart = 0;
  iBlock[0].yEnd   = N - 1;
  iBlock[0].zStart = 0;
  iBlock[0].zEnd   = N - 1;
  cerr = cudaMalloc(&fLocal_p[0], (N-1) * (N-1) * (N-1) * sizeof(double)); CUDA_CHK(cerr);
  cerr = cudaMalloc(&uLocal_p[0], (N-1) * (N-1) * (N-1) * sizeof(double)); CUDA_CHK(cerr);

  for (int i = 1; i < numDevices; i++) {
    iBlock[i].xStart = N - 1;
    iBlock[i].xEnd   = N - 1;
    iBlock[i].yStart = N - 1;
    iBlock[i].yEnd   = N - 1;
    iBlock[i].zStart = N - 1;
    iBlock[i].zEnd   = N - 1;
    fLocal_p[i] = NULL;
    uLocal_p[i] = NULL;
  }

  PetscFunctionReturn(0);
}

int GSRestoreFieldArraysDevice(GS gs, size_t N, int numDevices,
                               struct IndexBlock iBlock[],
                               double *fLocal_p[], double *uLocal_p[])
{
  cudaError_t    cerr;

  PetscFunctionBeginUser;
  cerr = cudaFree(fLocal_p[0]); CUDA_CHK(cerr);
  cerr = cudaFree(uLocal_p[0]); CUDA_CHK(cerr);
  PetscFunctionReturn(0);
}

int GSIterateDevice(GS gs, size_t N, int num_iter, int numDevices,
                    const struct IndexBlock iBlock[],
                    const double *fLocal[], double *uLocal[])
{
  PetscFunctionBeginUser;
  PetscFunctionReturn(0);
}
