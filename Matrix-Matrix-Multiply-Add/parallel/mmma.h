#if !defined(MMMA_H)
#define      MMMA_H

#include <stddef.h>
#include <mpi.h>
#include <petscviewer.h>
#include <petscblaslapack.h>
#include <petscmat.h>

#if defined(__cplusplus)
extern "C" {
#endif

typedef struct _mmma *MMMA;

/* This describes the layout of a partition of a matrix (whether a partition
 * of an MPI process or a cuda device).  It says that the partition includes
 * the entries in block at the rows [rowStart, ..., rowEnd - 1] and
 * the columsn [colStart, ..., colEnd - 1]
 */
struct MatrixBlock
{
  size_t rowStart, rowEnd;
  size_t colStart, colEnd;
};

/** Create a matrix matrix multiply add (mmma) context object.
 *
 * This object encapsulates all the decisions about how best to implement MMMA
 * for various number of processors and problem sizes
 *
 * comm: input, MPI communicator describing the parallel layout.
 * out:  mmma_p, the object that will control matrix-matrix-multiply-add
 * operations.
 */
int MMMACreate(MPI_Comm comm, MMMA *mmma_p);
/** Destroy the object */
int MMMADestroy(MMMA *mmma);

/** Get arrays for matrices to be filled.
  \param[in]     M      global row size of C & A
  \param[in]     N      global column size of C & B
  \param[in]     R      global column size of A / row size of B
  \param[out]    cBlock a declaration of which partition of the C matrix is on the current MPI process.
  \param[out]    aBlock a declaration of which partition of the A matrix is on the current MPI process.
  \param[out]    bBlock a declaration of which partition of the B matrix is on the current MPI process.
  \param[out]    CLocal_p  This should be made to point to an array, allocated by you, where you would like the local C matrix entries to be stored.
  \param[out]    ALocal_p  This should be made to point to an array, allocated by you, where you would like the local A matrix entries to be stored.
  \param[out]    BLocal_p  This should be made to point to an array, allocated by you, where you would like the local B matrix entries to be stored.
 */
int MMMAGetMatrixArraysDouble(MMMA mmma, size_t M, size_t N, size_t R,
                              struct MatrixBlock *cBlock,
                              struct MatrixBlock *aBlock,
                              struct MatrixBlock *bBlock,
                              double **CLocal_p, double **ALocal_p, double **BLocal_p);

/** Restore arrays allocated for matrices
  \param[in]     M      global row size of C & A
  \param[in]     N      global column size of C & B
  \param[in]     R      global column size of A / row size of B
  \param[in]     cBlock a declaration of which partition of the C matrix is on the current MPI process.
  \param[in]     aBlock a declaration of which partition of the A matrix is on the current MPI process.
  \param[in]     bBlock a declaration of which partition of the B matrix is on the current MPI process.
  \param[in/out] CLocal_p The array for the local block of C that you should now deallocate.
  \param[in/out] ALocal_p The array for the local block of A that you should now deallocate.
  \param[in/out] BLocal_p The array for the local block of B that you should now deallocate.
 */
int MMMARestoreMatrixArraysDouble(MMMA mmma, size_t M, size_t N, size_t R,
                                  struct MatrixBlock *cBlock,
                                  struct MatrixBlock *aBlock,
                                  struct MatrixBlock *bBlock,
                                  double **CLocal_p, double **ALocal_p, double **BLocal_p);
/** C := alpha * C + A * B.

  \param[in]     M      global row size of C & A
  \param[in]     N      global column size of C & B
  \param[in]     R      global column size of A / row size of B
  \param[in]     alpha  the multiplier
  \param[in]     cBlock a declaration of which partition of the C matrix is on the current MPI process.
  \param[in]     aBlock a declaration of which partition of the A matrix is on the current MPI process.
  \param[in]     bBlock a declaration of which partition of the B matrix is on the current MPI process.
  \param[in/out] C      the local partition of C.
  \param[in]     A      the local partition of A.
  \param[in]     B      the local partition of B.
*/
int MMMAApplyDouble(MMMA mmma, size_t M, size_t N, size_t R, double alpha,
                    const struct MatrixBlock *cBlock,
                    const struct MatrixBlock *aBlock,
                    const struct MatrixBlock *bBlock,
                    double *C, const double *A, const double *B);

/** Get arrays for matrices to be filled.
  \param[in]     M      global row size of C & A
  \param[in]     N      global column size of C & B
  \param[in]     R      global column size of A / row size of B
  \param[out]    cBlock a declaration of which partition of the C matrix is on the current MPI process.
  \param[out]    aBlock a declaration of which partition of the A matrix is on the current MPI process.
  \param[out]    bBlock a declaration of which partition of the B matrix is on the current MPI process.
  \param[out]    CLocal_p  This should be made to point to an array, allocated by you, where you would like the local C matrix entries to be stored.
  \param[out]    ALocal_p  This should be made to point to an array, allocated by you, where you would like the local A matrix entries to be stored.
  \param[out]    BLocal_p  This should be made to point to an array, allocated by you, where you would like the local B matrix entries to be stored.
 */
int MMMAGetMatrixArraysSingle(MMMA mmma, size_t M, size_t N, size_t R,
                              struct MatrixBlock *cBlock,
                              struct MatrixBlock *aBlock,
                              struct MatrixBlock *bBlock,
                              float **CLocal_p, float **ALocal_p, float **BLocal_p);
/** Restore arrays allocated for matrices
  \param[in]     M      global row size of C & A
  \param[in]     N      global column size of C & B
  \param[in]     R      global column size of A / row size of B
  \param[in]     cBlock a declaration of which partition of the C matrix is on the current MPI process.
  \param[in]     aBlock a declaration of which partition of the A matrix is on the current MPI process.
  \param[in]     bBlock a declaration of which partition of the B matrix is on the current MPI process.
  \param[in/out] CLocal_p The array for the local block of C that you should now deallocate.
  \param[in/out] ALocal_p The array for the local block of A that you should now deallocate.
  \param[in/out] BLocal_p The array for the local block of B that you should now deallocate.
 */
int MMMARestoreMatrixArraysSingle(MMMA mmma, size_t M, size_t N, size_t R,
                                  struct MatrixBlock *cBlock,
                                  struct MatrixBlock *aBlock,
                                  struct MatrixBlock *bBlock,
                                  float **CLocal_p, float **ALocal_p, float **BLocal_p);

/** C := alpha * C + A * B.

  \param[in]     M      global row size of C & A
  \param[in]     N      global column size of C & B
  \param[in]     R      global column size of A / row size of B
  \param[in]     alpha  the multiplier
  \param[in]     cBlock a declaration of which partition of the C matrix is on the current MPI process.
  \param[in]     aBlock a declaration of which partition of the A matrix is on the current MPI process.
  \param[in]     bBlock a declaration of which partition of the B matrix is on the current MPI process.
  \param[in/out] C      the local partition of C.
  \param[in]     A      the local partition of A.
  \param[in]     B      the local partition of B.
*/
int MMMAApplySingle(MMMA mmma, size_t M, size_t N, size_t R, float alpha,
                    const struct MatrixBlock *cBlock,
                    const struct MatrixBlock *aBlock,
                    const struct MatrixBlock *bBlock,
                    float *C, const float *A, const float *B,
                    PetscInt ncheck);

/** Get device arrays for matrices to be filled.  Only the memory that you
 * allocate in CLocal_p, ALocal_p, BLocal_p should be device memory, the other
 * inputs are host memory.

  \param[in]     M           global row size of C & A
  \param[in]     N           global column size of C & B
  \param[in]     R           global column size of A / row size of B
  \param[in]     numDevices  The number of devices available
  \param[out]    cBlock      an array of MatrixBlock structures of size `numDevices`.  You should fill this with the size of the partition of C for each device.
  \param[out]    aBlock      an array of MatrixBlock structures of size `numDevices`.  You should fill this with the size of the partition of A for each device.
  \param[out]    bBlock      an array of MatrixBlock structures of size `numDevices`.  You should fill this with the size of the partition of B for each device.
  \param[out]    CLocal_p    an array of pointers of size `numDevices`.  You should make each pointer point to **device memory** allocated for that device's partition of C.
  \param[out]    ALocal_p    an array of pointers of size `numDevices`.  You should make each pointer point to **device memory** allocated for that device's partition of A.
  \param[out]    BLocal_p    an array of pointers of size `numDevices`.  You should make each pointer point to **device memory** allocated for that device's partition of B.
 */
int MMMAGetMatrixArraysDoubleDevice(MMMA mmma, size_t M, size_t N, size_t R,
                                    int numDevices,
                                    struct MatrixBlock cBlock[],
                                    struct MatrixBlock aBlock[],
                                    struct MatrixBlock bBlock[],
                                    double *CLocal_p[], double *ALocal_p[], double *BLocal_p[]);
/** Restore arrays allocated for matrices

  \param[in]     M           global row size of C & A
  \param[in]     N           global column size of C & B
  \param[in]     R           global column size of A / row size of B
  \param[in]     numDevices  The number of devices available
  \param[out]    cBlock      an array of MatrixBlock structures of size `numDevices`.
  \param[out]    aBlock      an array of MatrixBlock structures of size `numDevices`.
  \param[out]    bBlock      an array of MatrixBlock structures of size `numDevices`.
  \param[out]    CLocal_p    an array of pointers of size `numDevices`.  You should deallocate the **device memory** allocated for that device's partition of C.
  \param[out]    ALocal_p    an array of pointers of size `numDevices`.  You should deallocate the **device memory** allocated for that device's partition of A.
  \param[out]    BLocal_p    an array of pointers of size `numDevices`.  You should deallocate the **device memory** allocated for that device's partition of B.
 */
int MMMARestoreMatrixArraysDoubleDevice(MMMA mmma, size_t M, size_t N, size_t R,
                                        int numDevices,
                                        struct MatrixBlock cBlock[],
                                        struct MatrixBlock aBlock[],
                                        struct MatrixBlock bBlock[],
                                        double *CLocal_p[], double *ALocal_p[], double *BLocal_p[]);

/** C := alpha * C + A * B.

  \param[in]     M          global row size of C & A
  \param[in]     N          global column size of C & B
  \param[in]     R          global column size of A / row size of B
  \param[in]     alpha      the multiplier
  \param[in]     numDevices the number of cuda devices used
  \param[in]     cBlock     a declaration of which partition of the C matrix is on each cuda device.
  \param[in]     aBlock     a declaration of which partition of the A matrix is on each cuda device.
  \param[in]     bBlock     a declaration of which partition of the B matrix is on each cuda device.
  \param[in/out] C          the local partition of C in device memory for each device.
  \param[in]     C          the local partition of A in device memory for each device.
  \param[in]     C          the local partition of B in device memory for each device.
*/
int MMMAApplyDoubleDevice(MMMA mmma, size_t M, size_t N, size_t R, double alpha,
                          int numDevices,
                          const struct MatrixBlock cBlock[],
                          const struct MatrixBlock aBlock[],
                          const struct MatrixBlock bBlock[],
                          double *C[], const double *A[], const double *B[]);

/** Get device arrays for matrices to be filled.  Only the memory that you
 * allocate in CLocal_p, ALocal_p, BLocal_p should be device memory, the other
 * inputs are host memory.

  \param[in]     M           global row size of C & A
  \param[in]     N           global column size of C & B
  \param[in]     R           global column size of A / row size of B
  \param[in]     numDevices  The number of devices available
  \param[out]    cBlock      an array of MatrixBlock structures of size `numDevices`.  You should fill this with the size of the partition of C for each device.
  \param[out]    aBlock      an array of MatrixBlock structures of size `numDevices`.  You should fill this with the size of the partition of A for each device.
  \param[out]    bBlock      an array of MatrixBlock structures of size `numDevices`.  You should fill this with the size of the partition of B for each device.
  \param[out]    CLocal_p    an array of pointers of size `numDevices`.  You should make each pointer point to **device memory** allocated for that devices partition of C.
  \param[out]    ALocal_p    an array of pointers of size `numDevices`.  You should make each pointer point to **device memory** allocated for that devices partition of A.
  \param[out]    BLocal_p    an array of pointers of size `numDevices`.  You should make each pointer point to **device memory** allocated for that devices partition of B.
 */
int MMMAGetMatrixArraysSingleDevice(MMMA mmma, size_t M, size_t N, size_t R,
                                    int numDevices,
                                    struct MatrixBlock cBlock[],
                                    struct MatrixBlock aBlock[],
                                    struct MatrixBlock bBlock[],
                                    float *CLocal_p[], float *ALocal_p[], float *BLocal_p[]);

/** Restore arrays allocated for matrices

  \param[in]     M           global row size of C & A
  \param[in]     N           global column size of C & B
  \param[in]     R           global column size of A / row size of B
  \param[in]     numDevices  The number of devices available
  \param[out]    cBlock      an array of MatrixBlock structures of size `numDevices`.
  \param[out]    aBlock      an array of MatrixBlock structures of size `numDevices`.
  \param[out]    bBlock      an array of MatrixBlock structures of size `numDevices`.
  \param[out]    CLocal_p    an array of pointers of size `numDevices`.  You should deallocate the **device memory** allocated for that device's partition of C.
  \param[out]    ALocal_p    an array of pointers of size `numDevices`.  You should deallocate the **device memory** allocated for that device's partition of A.
  \param[out]    BLocal_p    an array of pointers of size `numDevices`.  You should deallocate the **device memory** allocated for that device's partition of B.
 */
int MMMARestoreMatrixArraysSingleDevice(MMMA mmma, size_t M, size_t N, size_t R, int numDevices,
                                        struct MatrixBlock cBlock[],
                                        struct MatrixBlock aBlock[],
                                        struct MatrixBlock bBlock[],
                                        float *CLocal_p[], float *ALocal_p[], float *BLocal_p[]);

/** C := alpha * C + A * B.

  \param[in]     M          global row size of C & A
  \param[in]     N          global column size of C & B
  \param[in]     R          global column size of A / row size of B
  \param[in]     alpha      the multiplier
  \param[in]     numDevices the number of cuda devices used
  \param[in]     cBlock     a declaration of which partition of the C matrix is on each cuda device.
  \param[in]     aBlock     a declaration of which partition of the A matrix is on each cuda device.
  \param[in]     bBlock     a declaration of which partition of the B matrix is on each cuda device.
  \param[in/out] C          the local partition of C in device memory for each device.
  \param[in]     C          the local partition of A in device memory for each device.
  \param[in]     C          the local partition of B in device memory for each device.
*/
int MMMAApplySingleDevice(MMMA mmma, size_t M, size_t N, size_t R, float alpha,
                          int numDevices,
                          const struct MatrixBlock cBlock[],
                          const struct MatrixBlock aBlock[],
                          const struct MatrixBlock bBlock[],
                          float *C[], const float *A[], const float *B[]);

#if defined(__cplusplus)
}
#endif
#endif
