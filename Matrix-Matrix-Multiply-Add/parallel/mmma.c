#include "mmma.h"
#include <petscsys.h>
#include <petscviewer.h>

struct _mmma
{
  MPI_Comm comm;
};

int MMMACreate(MPI_Comm comm, MMMA *mmma_p)
{
  MMMA mmma = NULL;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscCalloc1(1,&mmma); CHKERRQ(ierr);

  mmma->comm = comm;

  *mmma_p = mmma;
  PetscFunctionReturn(0);
}

int MMMADestroy(MMMA *mmma_p)
{
  PetscErrorCode ierr;
  PetscFunctionBeginUser;

  ierr = PetscFree(*mmma_p); CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

int MMMAGetMatrixArraysDouble(MMMA mmma, size_t M, size_t N, size_t R,
                              struct MatrixBlock *cBlock,
                              struct MatrixBlock *aBlock,
                              struct MatrixBlock *bBlock,
                              double **CLocal_p, double **ALocal_p, double **BLocal_p)
{
  MPI_Comm comm;
  int      size, rank;
  PetscInt nClocal, nAlocal, nBlocal;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  comm = mmma->comm;

  ierr = MPI_Comm_size(comm, &size); CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm, &rank); CHKERRQ(ierr);

  cBlock->rowStart = (rank * M) / size;
  cBlock->rowEnd   = ((rank + 1) * M) / size;
  cBlock->colStart = 0;
  cBlock->colEnd   = N;
  nClocal = (cBlock->rowEnd - cBlock->rowStart) * (cBlock->colEnd - cBlock->colStart);

  aBlock->rowStart = (rank * M) / size;
  aBlock->rowEnd   = ((rank + 1) * M) / size;
  aBlock->colStart = 0;
  aBlock->colEnd   = R;
  nAlocal = (aBlock->rowEnd - aBlock->rowStart) * (aBlock->colEnd - aBlock->colStart);

  bBlock->rowStart = (rank * R) / size;
  bBlock->rowEnd   = ((rank + 1) * R) / size;
  bBlock->colStart = 0;
  bBlock->colEnd   = N;
  nBlocal = (bBlock->rowEnd - bBlock->rowStart) * (bBlock->colEnd - bBlock->colStart);

  ierr = PetscMalloc3(nClocal, CLocal_p, nAlocal, ALocal_p, nBlocal, BLocal_p); CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

int MMMARestoreMatrixArraysDouble(MMMA mmma, size_t M, size_t N, size_t R,
                                  struct MatrixBlock *cBlock,
                                  struct MatrixBlock *aBlock,
                                  struct MatrixBlock *bBlock,
                                  double **CLocal_p, double **ALocal_p, double **BLocal_p)
{
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscFree3(*CLocal_p, *ALocal_p, *BLocal_p); CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

int MMMAGetMatrixArraysSingle(MMMA mmma, size_t M, size_t N, size_t R,
                              struct MatrixBlock *cBlock,
                              struct MatrixBlock *aBlock,
                              struct MatrixBlock *bBlock,
                              float **CLocal_p, float **ALocal_p, float **BLocal_p)
{
  MPI_Comm comm;
  int      size, rank;
  PetscInt nClocal, nAlocal, nBlocal;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  comm = mmma->comm;

  ierr = MPI_Comm_size(comm, &size); CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm, &rank); CHKERRQ(ierr);

  cBlock->rowStart = (rank * M) / size;
  cBlock->rowEnd   = ((rank + 1) * M) / size;
  cBlock->colStart = 0;
  cBlock->colEnd   = N;
  nClocal = (cBlock->rowEnd - cBlock->rowStart) * (cBlock->colEnd - cBlock->colStart);

  aBlock->rowStart = (rank * M) / size;
  aBlock->rowEnd   = ((rank + 1) * M) / size;
  aBlock->colStart = 0;
  aBlock->colEnd   = R;
  nAlocal = (aBlock->rowEnd - aBlock->rowStart) * (aBlock->colEnd - aBlock->colStart);

  bBlock->rowStart = (rank * R) / size;
  bBlock->rowEnd   = ((rank + 1) * R) / size;
  bBlock->colStart = 0;
  bBlock->colEnd   = N;
  nBlocal = (bBlock->rowEnd - bBlock->rowStart) * (bBlock->colEnd - bBlock->colStart);

  ierr = PetscMalloc3(nClocal, CLocal_p, nAlocal, ALocal_p, nBlocal, BLocal_p); CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

int MMMARestoreMatrixArraysSingle(MMMA mmma, size_t M, size_t N, size_t R,
                                  struct MatrixBlock *cBlock,
                                  struct MatrixBlock *aBlock,
                                  struct MatrixBlock *bBlock,
                                  float **CLocal_p, float **ALocal_p, float **BLocal_p)
{
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscFree3(*CLocal_p, *ALocal_p, *BLocal_p); CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

int MMMAApplyDouble(MMMA mmma, size_t M, size_t N, size_t R, double alpha,
                    const struct MatrixBlock *cBlock,
                    const struct MatrixBlock *aBlock,
                    const struct MatrixBlock *bBlock,
                    double *C, const double *A, const double *B)
{
  return 0;
}


int MMMAApplySingle(MMMA mmma, size_t M, size_t N, size_t R, float alpha,
                    const struct MatrixBlock *cBlock,
                    const struct MatrixBlock *aBlock,
                    const struct MatrixBlock *bBlock,
                    float *C, const float *A, const float *B,
                    PetscInt ncheck)
{
  MPI_Comm comm;
  MPI_Request req[4];
  MPI_Status status[4];
  int      size, rank, r_prev, r_next;
  comm = mmma->comm;
  struct MatrixBlock *bBlock1 = NULL;
  struct MatrixBlock *bBlock2 = NULL; // previous block
  PetscErrorCode ierr;
  PetscViewer    viewer;
  viewer = PETSC_VIEWER_STDOUT_WORLD;
  PetscInt nBlocal;

  ierr = MPI_Comm_size(comm, &size); CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm, &rank); CHKERRQ(ierr);
  ierr = PetscMalloc1(1, &bBlock1); CHKERRQ(ierr);
  ierr = PetscMalloc1(1, &bBlock2); CHKERRQ(ierr);
  *bBlock1 = *bBlock;
  
  float *B1, *B2;
  int recvcounts;
  PetscInt BlocalMaxsize;
  BlocalMaxsize = N*((R/size)+1);
  if (ncheck==0){
      ierr = PetscViewerASCIIPrintf(viewer, "BlocalMaxsize: %D   \n", BlocalMaxsize);CHKERRQ(ierr);
    }
  ierr = PetscMalloc1(BlocalMaxsize, &B1); CHKERRQ(ierr);
  ierr = PetscMalloc1(BlocalMaxsize, &B2); CHKERRQ(ierr);
  nBlocal = (bBlock->colEnd - bBlock->colStart) * (bBlock->rowEnd - bBlock->rowStart);
  ierr = PetscMemcpy(B1, B, nBlocal * sizeof (PetscScalar)); CHKERRQ(ierr);

  for (size_t i = 0; i < (cBlock->rowEnd - cBlock->rowStart); i++) {
    for (size_t j = 0; j < N; j++) {
      C[i*N+j] = alpha * C[i*N+j];
    }
  }

  r_prev = (rank+size-1)%size;
  r_next = (rank+1)%size;
  size_t j_block;
  for (int p = 0; p < size; p++) {
    
    for (size_t i = 0; i < (aBlock->rowEnd - aBlock->rowStart); i++) {
      for (size_t j = bBlock1->rowStart; j < bBlock1->rowEnd; j++) {
        j_block = j - bBlock1->rowStart;
        for (size_t k = 0; k < N; k++){
          C[i*N + k] += A[i*R+j] * B1[j_block*N+k];
        }
      }
    }
    // send bBlock to next
    MPI_Isend(bBlock1, sizeof(struct MatrixBlock), MPI_BYTE, r_next, p, comm, &req[0]);
    // receive bBlock from previous
    MPI_Recv(bBlock2, sizeof(struct MatrixBlock), MPI_BYTE, r_prev, p, comm, &status[0]);
    MPI_Wait(&req[0], &status[2]);
    if (ncheck==0){
      ierr = PetscViewerASCIIPrintf(viewer, "received bBlock2->rowStart: %D    bBlock2->rowEnd: %D\n", bBlock2->rowStart, bBlock2->rowEnd);CHKERRQ(ierr);
    }
    // send B1 to next
    MPI_Isend(B1, (bBlock1->colEnd - bBlock1->colStart) * (bBlock1->rowEnd - bBlock1->rowStart), MPI_FLOAT, r_next, p+size, comm, &req[1]);
    // receive B2 from previous
    MPI_Recv(B2, (bBlock2->colEnd - bBlock2->colStart) * (bBlock2->rowEnd - bBlock2->rowStart), MPI_FLOAT, r_prev, p+size, comm, &status[1]);
    MPI_Wait(&req[1], &status[3]);
    // swap B1 and B2
    nBlocal = (bBlock2->colEnd - bBlock2->colStart) * (bBlock2->rowEnd - bBlock2->rowStart);
    if (ncheck==0){
      ierr = PetscViewerASCIIPrintf(viewer, " received nBlocal: %D\n", nBlocal);CHKERRQ(ierr);
    }
    ierr = PetscMemcpy(B1, B2, nBlocal * sizeof (PetscScalar)); CHKERRQ(ierr);
    if (ncheck==0){
      ierr = PetscViewerASCIIPrintf(viewer, "PetscScalar memory size (byte): %D   float memory size (byte): %D\n", nBlocal * sizeof (PetscScalar), nBlocal * sizeof (float));CHKERRQ(ierr);
    }
    //for (size_t m = 0; m<(bBlock2->colEnd - bBlock2->colStart) * (bBlock2->rowEnd - bBlock2->rowStart); m++){
      //B1[m] = B2[m];
    //}
    // swap bBlock1 and bBlock2
    
    *bBlock1 = *bBlock2;

    MPI_Barrier(comm);

  }
  if (ncheck==0){
      ierr = PetscViewerASCIIPrintf(viewer, "comm size: %D\n", size);CHKERRQ(ierr);
    }
  

  return 0;
}
