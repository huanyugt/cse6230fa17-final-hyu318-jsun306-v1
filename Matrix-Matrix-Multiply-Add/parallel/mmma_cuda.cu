
#include <petscsys.h>
#include "mmma.h"

#define CUDA_CHK(cerr) do {cudaError_t _cerr = (cerr); if ((_cerr) != cudaSuccess) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_LIB,"Cuda error");} while(0)

int MMMAGetMatrixArraysDoubleDevice(MMMA mmma, size_t M, size_t N, size_t R,
                                    int numDevices,
                                    struct MatrixBlock cBlock[],
                                    struct MatrixBlock aBlock[],
                                    struct MatrixBlock bBlock[],
                                    double *CLocal_p[], double *ALocal_p[], double *BLocal_p[])
{
  PetscInt nClocal, nAlocal, nBlocal;
  cudaError_t       cerr;

  PetscFunctionBeginUser;

  cBlock[0].rowStart = 0;
  cBlock[0].rowEnd   = M;
  cBlock[0].colStart = 0;
  cBlock[0].colEnd   = N;
  nClocal = M * N;

  aBlock[0].rowStart = 0;
  aBlock[0].rowEnd   = M;
  aBlock[0].colStart = 0;
  aBlock[0].colEnd   = R;
  nAlocal = M * R;

  bBlock[0].rowStart = 0;
  bBlock[0].rowEnd   = R;
  bBlock[0].colStart = 0;
  bBlock[0].colEnd   = N;
  nBlocal = R * N;

  cerr = cudaMalloc(&CLocal_p[0], nClocal * sizeof(double)); CUDA_CHK(cerr);
  cerr = cudaMalloc(&ALocal_p[0], nAlocal * sizeof(double)); CUDA_CHK(cerr);
  cerr = cudaMalloc(&BLocal_p[0], nBlocal * sizeof(double)); CUDA_CHK(cerr);

  for (int i = 1; i < numDevices; i++) {
    cBlock[i].rowStart = M;
    cBlock[i].rowEnd   = M;
    cBlock[i].colStart = N;
    cBlock[i].colEnd   = N;
    aBlock[i].rowStart = M;
    aBlock[i].rowEnd   = M;
    aBlock[i].colStart = R;
    aBlock[i].colEnd   = R;
    bBlock[i].rowStart = R;
    bBlock[i].rowEnd   = R;
    bBlock[i].colStart = N;
    bBlock[i].colEnd   = N;
    CLocal_p[i] = NULL;
    ALocal_p[i] = NULL;
    BLocal_p[i] = NULL;
  }

  PetscFunctionReturn(0);
}

int MMMARestoreMatrixArraysDoubleDevice(MMMA mmma, size_t M, size_t N, size_t R,
                                        int numDevices,
                                        struct MatrixBlock cBlock[],
                                        struct MatrixBlock aBlock[],
                                        struct MatrixBlock bBlock[],
                                        double *CLocal_p[], double *ALocal_p[], double *BLocal_p[])
{
  cudaError_t    cerr;

  PetscFunctionBeginUser;
  cerr = cudaFree(CLocal_p[0]); CUDA_CHK(cerr);
  cerr = cudaFree(ALocal_p[0]); CUDA_CHK(cerr);
  cerr = cudaFree(BLocal_p[0]); CUDA_CHK(cerr);
  PetscFunctionReturn(0);
}

int MMMAGetMatrixArraysSingleDevice(MMMA mmma, size_t M, size_t N, size_t R,
                                    int numDevices,
                                    struct MatrixBlock cBlock[],
                                    struct MatrixBlock aBlock[],
                                    struct MatrixBlock bBlock[],
                                    float *CLocal_p[], float *ALocal_p[], float *BLocal_p[])
{
  PetscInt nClocal, nAlocal, nBlocal;
  cudaError_t    cerr;

  PetscFunctionBeginUser;

  cBlock[0].rowStart = 0;
  cBlock[0].rowEnd   = M;
  cBlock[0].colStart = 0;
  cBlock[0].colEnd   = N;
  nClocal = M * N;

  aBlock[0].rowStart = 0;
  aBlock[0].rowEnd   = M;
  aBlock[0].colStart = 0;
  aBlock[0].colEnd   = R;
  nAlocal = M * R;

  bBlock[0].rowStart = 0;
  bBlock[0].rowEnd   = R;
  bBlock[0].colStart = 0;
  bBlock[0].colEnd   = N;
  nBlocal = R * N;

  cerr = cudaMalloc(&CLocal_p[0], nClocal * sizeof(float)); CUDA_CHK(cerr);
  cerr = cudaMalloc(&ALocal_p[0], nAlocal * sizeof(float)); CUDA_CHK(cerr);
  cerr = cudaMalloc(&BLocal_p[0], nBlocal * sizeof(float)); CUDA_CHK(cerr);

  for (int i = 1; i < numDevices; i++) {
    cBlock[i].rowStart = M;
    cBlock[i].rowEnd   = M;
    cBlock[i].colStart = N;
    cBlock[i].colEnd   = N;
    aBlock[i].rowStart = M;
    aBlock[i].rowEnd   = M;
    aBlock[i].colStart = R;
    aBlock[i].colEnd   = R;
    bBlock[i].rowStart = R;
    bBlock[i].rowEnd   = R;
    bBlock[i].colStart = N;
    bBlock[i].colEnd   = N;
    CLocal_p[i] = NULL;
    ALocal_p[i] = NULL;
    BLocal_p[i] = NULL;
  }

  PetscFunctionReturn(0);
}

int MMMARestoreMatrixArraysSingleDevice(MMMA mmma, size_t M, size_t N, size_t R,
                                        int numDevices,
                                        struct MatrixBlock cBlock[],
                                        struct MatrixBlock aBlock[],
                                        struct MatrixBlock bBlock[],
                                        float *CLocal_p[], float *ALocal_p[], float *BLocal_p[])
{
  cudaError_t    cerr;

  PetscFunctionBeginUser;
  cerr = cudaFree(CLocal_p[0]); CUDA_CHK(cerr);
  cerr = cudaFree(ALocal_p[0]); CUDA_CHK(cerr);
  cerr = cudaFree(BLocal_p[0]); CUDA_CHK(cerr);
  PetscFunctionReturn(0);
}

int MMMAApplyDoubleDevice(MMMA, size_t M, size_t N, size_t R, double alpha, int numDevices,
                          const struct MatrixBlock cBlock[],
                          const struct MatrixBlock aBlock[],
                          const struct MatrixBlock bBlock[],
                          double *Clocal[], const double *Alocal[], const double *Blocal[])
{
  return 0;
}

int MMMAApplySingleDevice(MMMA mmma, size_t M, size_t N, size_t R, float alpha, int numDevices,
                          const struct MatrixBlock cBlock[],
                          const struct MatrixBlock aBlock[],
                          const struct MatrixBlock bBlock[],
                          float *Clocal[], const float *Alocal[], const float *Blocal[])
{
  return 0;
}
/* vi: set expandtab sw=2 ts=2 cindent: */
