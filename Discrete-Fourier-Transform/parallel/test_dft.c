const char help[] = "Test driver for the correctness of discrete Fourier transform implementation";

#include <petscviewer.h>
#include <fftw3.h>
#include "dft.h"

int main(int argc, char **argv)
{
  PetscInt       test, numTests = 10;
  PetscInt       scale = 15;
  PetscRandom    rand;
  MPI_Comm       comm;
  PetscViewer    viewer;
  PetscErrorCode ierr;

  ierr = PetscInitialize(&argc, &argv, NULL, help); if (ierr) return ierr;
  comm = PETSC_COMM_WORLD;
  viewer = PETSC_VIEWER_STDOUT_WORLD;
  ierr = PetscOptionsBegin(comm, NULL, "Discrete Fourier Test Options", "test_dft.c");CHKERRQ(ierr);
  ierr = PetscOptionsInt("-num_tests", "Number of tests to run", "test_dft.c", numTests, &numTests, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-scale", "Scale (log2) of the array in the test", "test_dft.c", scale, &scale, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);

  ierr = PetscRandomCreate(comm, &rand);CHKERRQ(ierr);
  ierr = PetscRandomSetFromOptions(rand);CHKERRQ(ierr);
  ierr = PetscRandomSeed(rand);CHKERRQ(ierr);

  ierr = PetscViewerASCIIPrintf(viewer, "Running %D tests of discrete_fourier_transform()\n", numTests);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);
  for (test = 0; test < numTests; test++) {
    PetscComplex *x, *y, *ycheck;
    PetscReal     diff;
    PetscReal     norm;
    PetscInt      n, i;
    int           size, rank;
    size_t        localSize;
    struct IndexBlock ib;
    DFT           dft = NULL;

    ierr = PetscViewerASCIIPrintf(viewer, "Test %D:\n", test);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);

    n = 1 << (scale / 3);
    norm = PetscPowReal((PetscReal) n, -3./2.);

    ierr = PetscViewerASCIIPrintf(viewer, "Test dimensions: [%D x %D x %D]\n", n, n, n);CHKERRQ(ierr);

    ierr = DFTCreate(comm, &dft);CHKERRQ(ierr);
    ierr = DFTGetFieldArrays(dft, n, &ib, &x, &y); CHKERRQ(ierr);

    localSize = (ib.xEnd - ib.xStart) * (ib.yEnd - ib.yStart) * (ib.zEnd - ib.zStart);
    ierr = PetscMalloc1(localSize, &ycheck);CHKERRQ(ierr);

    ierr = PetscRandomSetInterval(rand, -1. + -1. * PETSC_i, 1. + 1 * PETSC_i);CHKERRQ(ierr);

    for (i = 0; i < localSize; i++) {
      PetscReal re, im;

      ierr = PetscRandomGetValueReal(rand, &re);CHKERRQ(ierr);
      ierr = PetscRandomGetValueReal(rand, &im);CHKERRQ(ierr);
      x[i] = re + PETSC_i * im;
    }

    ierr = DFTTransform(dft, n, &ib, x, y);CHKERRQ(ierr);

    {
      int64_t *localSizes = NULL;
      struct  IndexBlock *blockGlobal = NULL;
      int64_t i64localSize = localSize;
      PetscComplex *globalx = NULL, *globaly = NULL;
      MPI_Request sendreq, recvreq;

      ierr = MPI_Comm_size(comm, &size); CHKERRQ(ierr);
      ierr = MPI_Comm_rank(comm, &rank); CHKERRQ(ierr);
      if (!rank) {
        ierr = PetscMalloc2(size, &localSizes, size, &blockGlobal); CHKERRQ(ierr);
      }
      ierr = MPI_Gather(&i64localSize, 1, MPI_INT64_T, localSizes, 1, MPI_INT64_T, 0, comm); CHKERRQ(ierr);
      ierr = MPI_Gather(&ib, sizeof(struct IndexBlock), MPI_BYTE, blockGlobal, sizeof(struct IndexBlock), MPI_BYTE, 0, PETSC_COMM_WORLD); CHKERRQ(ierr);
      if (!rank) {

        ierr = PetscMalloc2(n*n*n, &globalx, n*n*n, &globaly); CHKERRQ(ierr);

      }
      ierr = MPI_Isend(x,localSize, MPIU_COMPLEX, 0, 0, comm, &sendreq); CHKERRQ(ierr);
      ierr = MPI_Irecv(ycheck,localSize, MPIU_COMPLEX, 0, 1, comm, &recvreq); CHKERRQ(ierr);

      if (!rank) {
        for (int q = 0; q < size; q++) {
          int bufferSize = localSizes[q];
          size_t offset;
          PetscComplex *buffer;

          ierr = PetscMalloc1(bufferSize, &buffer); CHKERRQ(ierr);
          ierr = MPI_Recv(buffer, bufferSize, MPIU_COMPLEX, q, 0, comm, MPI_STATUS_IGNORE); CHKERRQ(ierr);

          offset = 0;
          for (size_t z = blockGlobal[q].zStart; z < blockGlobal[q].zEnd; z++) {
            for (size_t y = blockGlobal[q].yStart; y < blockGlobal[q].yEnd; y++) {
              for (size_t x = blockGlobal[q].xStart; x < blockGlobal[q].xEnd; x++, offset++) {
                globalx[x + n * (y + n * z)] = buffer[offset];
              }
            }
          }
          ierr = PetscFree(buffer);
        }
        {
          fftw_plan p;

          p = fftw_plan_dft_3d(n, n, n, (fftw_complex *) globalx, (fftw_complex *) globaly,FFTW_FORWARD, FFTW_ESTIMATE);

          fftw_execute(p);

          fftw_destroy_plan(p);
        }

        for (int q = 0; q < size; q++) {
          int bufferSize = localSizes[q];
          size_t offset;
          PetscComplex *buffer;

          ierr = PetscMalloc1(bufferSize, &buffer); CHKERRQ(ierr);

          offset = 0;
          for (size_t z = blockGlobal[q].zStart; z < blockGlobal[q].zEnd; z++) {
            for (size_t y = blockGlobal[q].yStart; y < blockGlobal[q].yEnd; y++) {
              for (size_t x = blockGlobal[q].xStart; x < blockGlobal[q].xEnd; x++, offset++) {
                buffer[offset] = globaly[x + n * (y + n * z)];
              }
            }
          }
          ierr = MPI_Send(buffer, bufferSize, MPIU_COMPLEX, q, 1, comm); CHKERRQ(ierr);

          ierr = PetscFree(buffer);
        }
        ierr = PetscFree2(globalx, globaly); CHKERRQ(ierr);
        ierr = PetscFree2(localSizes, blockGlobal); CHKERRQ(ierr);
      }

      ierr = MPI_Wait(&sendreq, MPI_STATUS_IGNORE); CHKERRQ(ierr);
      ierr = MPI_Wait(&recvreq, MPI_STATUS_IGNORE); CHKERRQ(ierr);
    }

    diff = 0.;
    for (i = 0; i < localSize; i++) {
      PetscComplex res = y[i] - norm * ycheck[i];

      diff += PetscRealPart(res * PetscConjComplex(res));
    }
    diff = PetscSqrtReal(diff);

    if (diff > PETSC_SMALL) SETERRQ3(PETSC_COMM_SELF, PETSC_ERR_LIB, "Test %D failed residual test at threshold %g with value %g\n", test, (double) PETSC_SMALL, (double) diff);

    ierr = PetscFree(ycheck);CHKERRQ(ierr);
    ierr = DFTRestoreFieldArrays(dft, n, &ib, &x, &y); CHKERRQ(ierr);
    ierr = DFTCreate(comm, &dft);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);
    ierr = PetscViewerASCIIPrintf(viewer, "Passed.\n");CHKERRQ(ierr);


    ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);
    ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);
  }
  ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);
  ierr = PetscRandomDestroy(&rand);CHKERRQ(ierr);

  ierr = PetscFinalize();
  return ierr;
}
